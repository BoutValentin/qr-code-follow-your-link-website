import { IMAGE_BG_CLEAR_RECTANGLE } from './image_display_helper';

export const calc_matrix_size = (container, matrix_size) => {
	if (container) {
		const cube_width_height = (container.offsetWidth / matrix_size).toFixed(3);
		const image_width_height = container.offsetWidth * 0.3 - 5;

		return { cube_width_height, image_width_height };
	}
	return { cube_width_height: null, image_width_height: null };
};

export const calc_image_clear_bg_in_matrix = (
	matrix,
	matrix_size,
	image_width_height,
	cube_width_height
) => {
	const new_matrix = JSON.parse(JSON.stringify(matrix));

	const centre = Math.round(matrix_size / 2);

	const number_of_cube = image_width_height / cube_width_height;
	const number_of_cube_by_side = Math.round(number_of_cube / 2);
	const start = centre - number_of_cube_by_side - 1;
	const max = centre + number_of_cube_by_side;

	for (let index = start; index < max; index++) {
		for (let index_2 = start; index_2 < max; index_2++) {
			new_matrix[index][index_2] = false;
		}
	}

	return { new_matrix };
};

export const calc_matrix_size_with_image_logic = (
	container,
	matrix,
	matrix_size,
	image_url,
	type_image
) => {
	const { cube_width_height, image_width_height } = calc_matrix_size(container, matrix_size);
	if (cube_width_height === null || image_width_height === null) {
		return {
			cube_width_height,
			image_width_height,
			new_matrix: JSON.parse(JSON.stringify(matrix))
		};
	}
	if (image_url && image_url !== '' && type_image === IMAGE_BG_CLEAR_RECTANGLE) {
		const { new_matrix } = calc_image_clear_bg_in_matrix(
			matrix,
			matrix_size,
			image_width_height,
			cube_width_height
		);
		return { cube_width_height, image_width_height, new_matrix };
	}
	return {
		cube_width_height,
		image_width_height,
		new_matrix: JSON.parse(JSON.stringify(matrix))
	};
};
