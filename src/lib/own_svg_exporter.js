import { DomFileSaver } from '$lib/file_saver.js';

// TODO: Implement SVG text (maybe waiting for svg textarea)

class SVGElement {
	constructor(name, properties, parent) {
		this.name = name;
		this.properties = properties;
		this.child = [];
		this.parent = parent;
		this.inner_value = '';
	}

	add_property(name, content, override = true) {
		if (this.properties.hasOwnProperty(name)) {
			if (!override) {
				this.properties[name].push(content);
			}
			return;
		}
		this.properties[name] = [content];
	}

	add_properties(properties = []) {
		for (const { name, content, override } of properties) {
			this.add_property(name, content, override);
		}
	}

	clear_properties(name) {
		delete this.properties[name];
	}

	add_child(svg) {
		this.child.push(svg);
	}

	create_child(name, properties = {}) {
		return this._create_child(() => new SVGElement(name, properties, this));
	}

	create_g(properties = {}) {
		return this._create_child(() => new GSVG(properties, this));
	}

	create_image(properties = {}) {
		return this._create_child(() => new ImageSVG(properties, this));
	}

	create_rect(h_w_x_y, properties = {}) {
		const c = new RectangleSVG(properties, this);
		c.add_default_properties(h_w_x_y);
		return this._create_child(() => c);
	}

	create_text(h_w_x_y, properties = {}) {
		const c = new TextSVG(properties, this);
		c.add_default_properties(h_w_x_y);
		return this._create_child(() => c);
	}

	_create_child(wrapper) {
		// Using wrapper to create a subchild;
		const c = wrapper();
		// Returning that child;
		return this._push_child(c);
	}

	_push_child(c) {
		this.child.push(c);
		return c;
	}

	open_properties_string() {
		let str = `<${this.name}`;
		Object.keys(this.properties).forEach((key) => {
			str += ` ${key}="${this.properties[key].join(' ')}"`;
		});
		return `${str}>`;
	}

	end_properties_string() {
		return `</${this.name}>\n`;
	}

	to_string() {
		let str = `${this.open_properties_string()}`;
		this.child.forEach((value) => {
			str += `${value.to_string()}`;
		});
		return `${str}${this.inner_value}${this.end_properties_string()}`;
	}

	// To be override and call
	parse_css_element_of_interest(css_computed_style) {
		this.add_property('fill', css_computed_style.getPropertyValue('background-color'));
		this.add_property('stroke-width', css_computed_style.getPropertyValue('border-width'));
		this.add_property('stroke', css_computed_style.getPropertyValue('border-color'));
	}
}

class GSVG extends SVGElement {
	constructor(properties, parent) {
		super('g', properties, parent);
	}
}

class ImageSVG extends SVGElement {
	constructor(properties, parent) {
		super('image', properties, parent);
	}
}

class TextSVG extends SVGElement {
	constructor(properties, parent) {
		super('text', properties, parent);
	}

	parse_css_element_of_interest(css_computed_style) {
		//super.parse_css_element_of_interest(css_computed_style);
		const font = `font: ${css_computed_style.getPropertyValue(
			'font-style'
		)} ${css_computed_style.getPropertyValue('font')};`;
		console.log(css_computed_style);
		this.add_property('style', `${font}`);
	}

	add_default_properties({ h, w, x, y }) {
		const properties = [
			{
				name: 'height',
				content: h,
				override: true
			},
			{
				name: 'width',
				content: w,
				override: true
			},
			{
				name: 'x',
				content: x,
				override: true
			},
			{
				name: 'y',
				content: y,
				override: true
			}
		];
		this.add_properties(properties);
	}
}

class RectangleSVG extends SVGElement {
	constructor(properties, parent) {
		super('rect', properties, parent);
		this.width = null;
		this.height = null;
		this.x = null;
		this.y = null;
	}

	parse_css_element_of_interest(css_computed_style) {
		super.parse_css_element_of_interest(css_computed_style);
		if (this.width) {
			const br = css_computed_style.getPropertyValue('border-radius');
			if (br.endsWith('%')) {
				let br_i = br.replace('%', '');
				br_i = parseInt(br_i);
				this.add_property('rx', (br_i / 100) * this.width);
			} else if (br.endsWith('px') && br !== '0px') {
				this.add_property('rx', br.replace('px', ''));
			}
		}

		//this.add_property('fill', css_computed_style.getPropertyValue('background-color'));
	}

	add_default_properties({ h, w, x, y }) {
		const properties = [
			{
				name: 'height',
				content: h,
				override: true
			},
			{
				name: 'width',
				content: w,
				override: true
			},
			{
				name: 'x',
				content: x,
				override: true
			},
			{
				name: 'y',
				content: y,
				override: true
			}
		];
		this.width = w;
		this.height = h;
		this.x = y;
		this.y = y;
		this.add_properties(properties);
	}
}

class SVGContainer {
	constructor() {
		const properties = {
			xmlns: ['http://www.w3.org/2000/svg'],
			'xmlns:xlink': ['http://www.w3.org/1999/xlink'],
			'data-stacking-context': ['true']
		};
		this.content = new SVGElement('svg', properties, null);
	}

	to_string() {
		return this.content.to_string();
	}

	to_blob() {
		return new Blob([this.to_string()], { type: 'image/svg+xml' });
	}
}

class DomParserSVG extends DomFileSaver {
	constructor(container) {
		super(container);
		this.svg_container = null;
		this.current_svg_node = null;
	}

	parser(element) {
		switch (element.nodeName) {
			case 'DIV':
				this.parse_div(element);
				break;
			case 'IMG':
				this.parse_img(element);
				break;
			case 'P':
				this.parse_text(element);
				break;
		}
	}

	parse_div(element) {
		const curr_svg_element = this.current_svg_node.create_g();

		this.current_svg_node = curr_svg_element;

		// TODO: parsing properties should be better
		if (element.classList.contains('rounded')) {
			const { x, y } = this._get_x_y_h_w(element);
			const circle = curr_svg_element.create_child('circle', {});
			const radius = element.offsetWidth / 2;
			circle.add_property('cx', x + radius);
			circle.add_property('cy', y + radius);
			circle.add_property('r', radius);
			circle.parse_css_element_of_interest(getComputedStyle(element, null));
		}
		for (const child of element.childNodes) {
			this.parser(child);
		}
		if (element.childNodes.length <= 0) {
			const x_y_h_w = this._get_x_y_h_w(element);
			const rect = curr_svg_element.create_rect(x_y_h_w);
			rect.parse_css_element_of_interest(getComputedStyle(element, null));
		}
		this.current_svg_node = curr_svg_element.parent;
	}

	getBase64Image(img) {
		const canvas = document.createElement('canvas');
		canvas.width = img.width;
		canvas.height = img.height;
		const ctx = canvas.getContext('2d');
		ctx.drawImage(img, 0, 0, img.width, img.height);
		const dataURL = canvas.toDataURL('image/png');
		canvas.remove();
		return dataURL;
	}

	parse_img(element) {
		const parent_image = this.current_svg_node.create_g({ role: ['img'] });
		const { x, y, w, h } = this._get_x_y_h_w(element);

		const image = this.getBase64Image(element);
		const r = parent_image.create_rect({ x, y, w, h });
		r.parse_css_element_of_interest(getComputedStyle(element, null));
		parent_image.create_image({ href: [image], width: [w], height: [h], x: [x], y: [y] });
		this.current_svg_node = parent_image.parent;
	}

	parse_text(element) {
		const parent_text = this.current_svg_node.create_g({ role: ['text'] });
		const x_y_h_w = this._get_x_y_h_w(element);
		const r = parent_text.create_rect(x_y_h_w);
		r.parse_css_element_of_interest(getComputedStyle(element, null));

		const text = parent_text.create_text(x_y_h_w);
		text.inner_value = element.innerText;
		text.parse_css_element_of_interest(getComputedStyle(element, null));
		this.parent_text = parent_text.parent;
	}

	create_inline_svg() {
		this.svg_container = new SVGContainer();
		const { x, y, w, h } = this._get_x_y_h_w(this.container);
		this.current_svg_node = this.svg_container.content;
		this.current_svg_node.add_property('width', w);
		this.current_svg_node.add_property('height', h);
		this.current_svg_node.add_property('viewBox', `${x} ${y} ${w} ${h}`);

		this.parser(this.container);
		return this.svg_container;
	}

	// Overwrite
	save_as_file_inline_svg(file_name = 'qr-code.svg') {
		if (!this.container) return;
		const svg_container = this.create_inline_svg();
		const svg_str = svg_container.to_string();
		this.save_file(svg_str, 'image/svg+xml', file_name);
	}

	// Private
	_get_x_y_h_w(element) {
		const { x, y, width, height } = element.getBoundingClientRect();
		return { x, y, w: width, h: height };
	}
}

export const file_saver_svg_helper = (container, filename) => {
	if (!container) return;
	const parser = new DomParserSVG(container);
	parser.save_as_file_inline_svg(filename);
};
