import domtoimage from 'dom-to-image';

export class FileSaver {
	save_file(content, type, filename) {
		const blob = new Blob([content], { type });
		this.save_file_from_blob(blob, filename);
	}

	save_file_from_blob(blob, filename) {
		this._save_file_from_wrapper(filename, () => window.URL.createObjectURL(blob));
	}

	save_file_from_text_as_data(content, filename) {
		this._save_file_from_wrapper(filename, () => content);
	}

	_save_file_from_wrapper(filename, wrapper) {
		const a_tag = document.createElement('a');
		a_tag.download = filename;
		a_tag.href = wrapper();
		a_tag.target = '_blank';
		a_tag.click();
		a_tag.remove();
	}
}

export class DomFileSaver extends FileSaver {
	constructor(container) {
		super();
		this.container = container;
	}

	save_as_file_inline_svg(file_name = 'qr-code.svg') {
		if (!this.container) return;
		// const svgDocument = elementToSVG(this.container);
		// inlineResources(svgDocument.documentElement).then(() => {
		// const svgString = new XMLSerializer().serializeToString(svgDocument);
		// this.save_file(svgString, 'image/svg+xml', file_name);
		// });
		domtoimage.toSvg(this.container).then((svg_str) => {
			const newsvg = svg_str.replace('data:image/svg+xml;charset=utf-8,', '');
			this.save_file(newsvg, 'image/svg+xml', file_name);
		});
	}

	save_as_file_png(file_name = 'qr-code.png') {
		if (!this.container) return;
		domtoimage.toBlob(this.container).then((blob) => {
			this.save_file_from_blob(blob, file_name);
		});
	}
}

export const file_saver_png_helper = (container, filename) => {
	if (!container) return;
	const parser = new DomFileSaver(container);
	parser.save_as_file_png(filename);
};

export const file_saver_svg_helper = (container, filename) => {
	if (!container) return;
	const parser = new DomFileSaver(container);
	parser.save_as_file_inline_svg(filename);
};

export const DEFAULT_QR_CODE_NAME = 'QR_CODE';
export const PNG_EXTENSION = '.png';
export const SVG_EXTENSION = '.svg';

export const DEFAULT_QR_CODE_NAME_PNG = DEFAULT_QR_CODE_NAME + PNG_EXTENSION;
export const DEFAULT_QR_CODE_NAME_SVG = DEFAULT_QR_CODE_NAME + SVG_EXTENSION;
